<?php

require_once '/app/vendor/autoload.php';
use CountryEnums\Country;

$countryCode = 'VU';
$country = Country::from($countryCode);
$regions = $country->regions();

echo "code: ". $country->code() ." label: ". $country->label(). "\n";
foreach($regions as $region) {
    echo "code: ". $region->code() ." label: ". $region->label(). "\n";
}
$timezones = DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, $countryCode);
echo "timezones: " .join(', ', $timezones). "\n";


